/**
 * Padding hex if needed
 * @param {string} comp 
 * @returns {string} two hexa chars
 */
const pad = (comp) => {
    let padded = comp
    if (comp.length < 2) {
        padded = "0" + comp;

    }
    return padded;

};

// src/converter.js

/**
 * RGB to HEX conversion
 * 
 * @param {number} r RED
 * @param {number} g GREEN
 * @param {number} b BLUE
 * @returns {string} in hex color format, e.g., "#00ff00" (green)
 */
export const rgb_to_hex = (r, g, b) => {
    let hex = "#";
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return hex + pad(HEX_RED) + pad(HEX_GREEN)+ pad(HEX_BLUE);
};

/**
 * Converts a hex color code to RGB.
 * 
 * @param {string} hex The hex color code, e.g., "#00ff00"
 * @returns {{r: number, g: number, b: number}} An object containing the RGB components
 */
export const hex_to_rgb = (hex) => {
    hex = hex.replace(/^#/, '');

    let r, g, b;
    if (hex.length === 3) {
        r = parseInt(hex.charAt(0) + hex.charAt(0), 16);
        g = parseInt(hex.charAt(1) + hex.charAt(1), 16);
        b = parseInt(hex.charAt(2) + hex.charAt(2), 16);
    } else {
        r = parseInt(hex.substring(0, 2), 16);
        g = parseInt(hex.substring(2, 4), 16);
        b = parseInt(hex.substring(4, 6), 16);
    }

    return { r, g, b };
};
